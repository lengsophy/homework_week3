/**
 * @format
 */
import {AppRegistry} from 'react-native';
import Homework from './src/components/Homework'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Homework);
