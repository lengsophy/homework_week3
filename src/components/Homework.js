import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableHighlight,
    Image,
    Alert,
  } from 'react-native';
export default class Homework extends Component {
    constructor(props) {
        super(props);
        state = {
          username   : '',
          password: '',
        }
      }
      onClickListener = (viewId) => {
        Alert.alert("WordPress", viewId);
      }
      render() {
        return (
          <View style={styles.container}>
            <Image source = {require('../../img/logo.jpg')} style={ styles.logoStyle }/>
            <View style={styles.inputContainer}>
              {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/message/ultraviolet/50/3498db'}}/> */}
              <TextInput style={styles.inputs}
                  placeholder="Username"
                  keyboardType="name-phone-pad"
                  underlineColorAndroid='transparent'
                  onChangeText={(username) => this.setState({username})}/>
            </View>
            
            <View style={styles.inputContainer}>
              {/* <Image style={styles.inputIcon} source={{uri: 'https://png.icons8.com/key-2/ultraviolet/50/3498db'}}/> */}
              <TextInput style={styles.inputs}
                  placeholder="Password"
                  secureTextEntry={true}
                  underlineColorAndroid='transparent'
                  onChangeText={(password) => this.setState({password})}/>
            </View> 
            <TouchableHighlight style={[styles.buttonContainer, styles.loginButton]} onPress={() => this.onClickListener('login failed')}>
              <Text style={styles.loginText}>Login</Text>
            </TouchableHighlight>
    
            <TouchableHighlight style={[styles.buttonContainer, styles.lostPasswordText]} onPress={() => this.onClickListener('Do you want to reset password!')}>
                <Text>Lost your password?</Text>
            </TouchableHighlight>
    
            <TouchableHighlight style={[styles.buttonContainer, styles.registerText]} onPress={() => this.onClickListener('register failed')}>
                <Text>Register</Text>
            </TouchableHighlight>
          </View>
        );
      }
    }
    
    const styles = StyleSheet.create({
      container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#DCDCDC',
      },
      inputContainer: {
          borderBottomColor: '#F5FCFF',
          backgroundColor: '#FFFFFF',
        //  borderRadius:30,
          borderBottomWidth: 1,
          width:250,
          height:45,
          marginBottom:20,
          flexDirection: 'row',
          alignItems:'center'
      },
      inputs:{
          height:45,
          marginLeft:16,
          borderBottomColor: '#FFFFFF',
          flex:1,
      },
      inputIcon:{
        width:30,
        height:30,
        marginLeft:15,
        justifyContent: 'center'
      },
      buttonContainer: {
        height:45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
       // marginBottom:20,
        width:250,
        borderRadius:30,
      },
      loginButton: {
        width:100,
        backgroundColor: "#00b5ec",
      },
      loginText: {
        color: 'white',
      },
      logoStyle: {
        width: 60,
        height: 60,
        borderRadius: 60/2,
        justifyContent: 'center',
      },
      lostPasswordText: {

      },
      registerText: {

      },
      checkboxStyle: {
        color: 'black',
        marginLeft:20,
        width: 20,
        height: 20
      }
});
    